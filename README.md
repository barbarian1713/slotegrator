# slotegrator



## UI

Сделал, как и было указано selenium+cucumber, хотя с помощью selenide было бы немного проще.<br>
Чтобы запустить, нужно стянуть репозиторий и запустить в идее файл Run (ui/src/test/java/Runners/Run)

## API

Чтобы запустить, нужно стянуть репозиторий, и запустить в идее файл TestSlotegratorApi (api/src/test/java/apiTests/TestSlotegratorApi)<br>
Начав делать первый тест, что-то пошло не так, и я решил попробовать сделать запрос через Postman.<br>
Я прописал все по документации, но в ответ получал сообщение "The authorization grant type is not supported by the authorization server. Check the `grant_type` parameter."<br>
Может я что-то недопонял по документации с этим полем, сделал как смог.<br>
Если там какая-то элементарная ошибка, буду благодарен, если Вы отпишите, что было не так.
