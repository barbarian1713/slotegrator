package config;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeClass;

import static constants.Constants.RunVeriable.path;
import static constants.Constants.RunVeriable.server;

public class TestConfig {

    protected ResponseSpecification responseSpecificationFor200 = new ResponseSpecBuilder()
            .expectStatusCode(200)
            .build();
    protected ResponseSpecification responseSpecificationFor201 = new ResponseSpecBuilder()
            .expectStatusCode(201)
            .build();

    protected ResponseSpecification responseSpecificationFor404 = new ResponseSpecBuilder()
            .expectStatusCode(404)
            .build();

    @BeforeClass
    public void setUp() {
        RestAssured.baseURI = server;
        RestAssured.basePath = path;
    }
}
