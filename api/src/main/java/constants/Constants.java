package constants;

import static constants.Constants.Path.TEST_PATH;
import static constants.Constants.Servers.TEST_URL;

public class Constants {
    public static class RunVeriable {
        public static String server = TEST_URL;
        public static String path = TEST_PATH;
    }

    public static class Servers {
        public static String TEST_URL = "http://test-api.d6.dev.devcaz.com/";
    }

    public static class Path {
        public static String TEST_PATH = "v2/";
    }

    public static class Actions {
        public static String TEST_AUTHORIZATION = "oauth2/token";
        public static String TEST_PROTECTED_RESOURCE_REQUEST = "players";
        public static String TEST_PLAYERS = "players";


    }
}
