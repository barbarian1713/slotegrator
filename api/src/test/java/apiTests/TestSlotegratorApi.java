package apiTests;

import config.TestConfig;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.util.Base64;
import java.util.Map;

import static constants.Constants.Actions.TEST_AUTHORIZATION;
import static constants.Constants.Actions.TEST_PLAYERS;
import static constants.Constants.RunVeriable.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;



public class TestSlotegratorApi extends TestConfig {

    public static String id;
    protected String accessTokenGuest;
    protected String refreshTokenGuest;
    protected String accessTokenPlayer;
    protected String refreshTokenPlayer;


    protected String basicAuthenticationUsername = "front_2d6b0a8391742f5d789d7d915755e09e";
    protected String encodedbasicAuthenticationUsername = "Basic " +Base64.getEncoder().encodeToString(basicAuthenticationUsername.getBytes());

    protected String originalLogin = "barbarian1713@gmail.com";
    protected String originalPassword = "123jojo";
    protected String encodedPassword = Base64.getEncoder().encodeToString(originalPassword.getBytes());
    protected String email = "barbarian1713@gmail.com";
    protected String name = "Alex";
    protected String surname = "Goryaev";
    protected String currencyCode = "EUR";

    @Test(priority = 1)
    public void GetGuestToken() {
        String jsonBodyClientCredentialsGrant = "{\n" +
                "\t\"grant_type\": \"client_credentials\",\n" +
                "\t\"scope\": \"guest:default\"\n" +
                "}";

        Response response =
                given().auth().basic(basicAuthenticationUsername,"").body(jsonBodyClientCredentialsGrant)
                        .header("Host",server+path+TEST_AUTHORIZATION)
                        .header("Content-Type","application/json")
                        .header("Authorization",encodedbasicAuthenticationUsername)
                        .log().uri().log().method().log().params().log().headers().log().body().
                when().post(TEST_AUTHORIZATION).
                then().spec(responseSpecificationFor200).body(matchesJsonSchemaInClasspath("jsonSchemaAuthorization.json")).
                        body("access_token", is(notNullValue())).
                        log().status().log().body().
                        extract().response();

        accessTokenGuest = response.getBody().path("access_token");
        refreshTokenGuest = response.getBody().path("refresh_token");
    }

    @Test(priority = 2)
    public void RegisterANewPlayer() {
        String jsonBodyRegisterANewPlayer = "{\n" +
                "\t\"username\": \""+originalLogin+"\",\n" +
                "\t\"password_change\": \""+encodedPassword+"\",\n" +
                "\t\"password_repeat\": \""+encodedPassword+"\",\n" +
                "\t\"email\": \""+email+"\",\n" +
                "\t\"name\": \""+name+"\",\n" +
                "\t\"surname\": \""+surname+"\",\n" +
                "\t\"currency_code\": \""+currencyCode+"\"}";

        Response response =
                given().auth().basic(basicAuthenticationUsername,"").body(jsonBodyRegisterANewPlayer)
                        .header("Authorization",encodedbasicAuthenticationUsername)
                        .log().uri().log().method().log().params().log().headers().log().body().
                        when().post(TEST_PLAYERS).
                        then().spec(responseSpecificationFor201).body(matchesJsonSchemaInClasspath("jsonSchemaRegisterANewPlayer.json")).
                        log().status().log().body().
                        extract().response();

        id = response.getBody().path("id");
    }

    @Test(priority = 3)
    public void GetPlayerToken() {
        String jsonBodyGetPlayerToken = "{\n" +
                "\t\"grant_type\": \"password\",\n" +
                "\t\"username\": \""+name+"\",\n" +
                "\t\"password\": \""+encodedPassword+"\"\n" +
                "}";

        Response response =
                given().auth().basic(basicAuthenticationUsername,"").body(jsonBodyGetPlayerToken)
                        .header("Host",server+path+TEST_AUTHORIZATION)
                        .header("Content-Type","application/json")
                        .header("Authorization",encodedbasicAuthenticationUsername)
                        .log().uri().log().method().log().params().log().headers().log().body().
                        when().post(TEST_AUTHORIZATION).
                        then().spec(responseSpecificationFor200).body(matchesJsonSchemaInClasspath("jsonSchemaAuthorization.json")).
                        body("access_token", is(notNullValue())).
                        log().status().log().body().
                        extract().response();

        accessTokenPlayer = response.getBody().path("access_token");
        refreshTokenPlayer = response.getBody().path("refresh_token");
    }

    @Test(priority = 4)
    public void GetSinglePlayerCorrectProfile() {
                given().auth().basic(basicAuthenticationUsername,"")
                        .header("Authorization",encodedbasicAuthenticationUsername)
                        .log().uri().log().method().log().params().log().headers().log().body().
                        when().get(TEST_PLAYERS+"/"+id).
                        then().spec(responseSpecificationFor200).body(matchesJsonSchemaInClasspath("jsonSchemaGetSinglePlayerProfile.json")).
                        log().status().log().body();
    }

    @Test(priority = 5)
    public void GetSinglePlayerIncorrectProfile() {
        given().auth().basic(basicAuthenticationUsername,"")
                .header("Authorization",encodedbasicAuthenticationUsername)
                .log().uri().log().method().log().params().log().headers().log().body().
                when().get(TEST_PLAYERS+"/2").
                then().spec(responseSpecificationFor404).body(matchesJsonSchemaInClasspath("jsonSchemaGetSinglePlayerProfile.json")).
                log().status().log().body();
    }


}
