package Common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static Constants.constant.TimeoutVariables.IMPLICIT_WAIT;

public class commonActions {
    public static WebDriver createDriver() {
        WebDriver driver = null;
        System.setProperty("webdriver.cgrome.driver","src/main/resources/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, TimeUnit.SECONDS);
        return driver;
    }
}
