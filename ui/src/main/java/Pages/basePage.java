package Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static Constants.constant.TimeoutVariables.EXPLICIT_WAIT;

public class basePage {

    WebDriver driver;

    public basePage(WebDriver driver) {
        this.driver = driver;
    }

    public void goToUrl (String url){
        driver.get(url);
    }

    public void clearAndType (WebElement element,String value){
        while (!element.getAttribute("value").equals("")) element.sendKeys(Keys.BACK_SPACE);
        element.sendKeys(value);
    }


}
