package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;


import java.time.Duration;

import static Constants.constant.BaseVariables.*;
import static com.sun.org.apache.xalan.internal.xsltc.runtime.BasisLibrary.*;

public class loginPage extends basePage {


    private final By loginLoc = By.xpath("//input[@placeholder=\"Login\"]");

    private final By passwordLoc = By.xpath("//input[@placeholder=\"Password\"]");
    private final By submitButton = By.xpath("//input[@type=\"submit\"]");
    private final By headerLogo = By.xpath("//img[@id=\"header-logo\"]");

    private final By usersLoc = By.xpath("(//i[@class=\"fa fa-user\"])[2]");
    private final By playersLoc = By.xpath("//a[text()=\"Players\"]");

    private final By tableLoc = By.xpath("(//div[@class=\"col-md-12\"])[2]");
    private final By sortByName = By.xpath("(//a[@class=\"sort-link\"])[1]");


    public loginPage(WebDriver driver) {
        super(driver);
    }

    public void enterValidLogin() {
        clearAndType(driver.findElement(loginLoc), LOGIN_SLOTEGRATOR);
    }

    public void enterValidPassword() {
        clearAndType(driver.findElement(passwordLoc), PASSWORD_SLOTEGRATOR);
    }

    public void clickSubmit() {
        driver.findElement(submitButton).click();
    }

    public void headerLogoIsVisible() {
        Assert.assertTrue(driver.findElement(headerLogo).isEnabled());
    }

    public void clickUsers() {
        driver.findElement(usersLoc).click();
    }

    public void clickPlayers() {
        driver.findElement(playersLoc).click();
    }

    public void checkTableIsVisible() {
        Assert.assertTrue(driver.findElement(tableLoc).isEnabled());
    }

    public void clickSortPlayersByName() {
        Assert.assertTrue(driver.findElement(sortByName).isEnabled());
        driver.findElement(sortByName).click();
    }

    public void checkTableSorted() {
        Assert.assertTrue(driver.findElement(sortByName).isEnabled());
        driver.navigate().refresh();
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10000));
        int i =1;
        int ii=i+1;
        Boolean sort;
        int compare;
        do {
            WebElement name1 = driver.findElement( By.xpath("(\t//td/a[contains(@href,\"/\") and not(contains(@class,\"btn\"))]\n)["+i+"]"));
            WebElement name2 = driver.findElement( By.xpath("(\t//td/a[contains(@href,\"/\") and not(contains(@class,\"btn\"))]\n)["+ii+"]"));
            String value1 = name1.getText();
            String value2 = name2.getText();

            compare= value1.compareTo(value2);

            if (compare<0){
                sort=true;
            }else {
                sort=false;
            }
            Assert.assertTrue(sort);
            i++;
            ii++;
        } while (i<5);

   }

}
