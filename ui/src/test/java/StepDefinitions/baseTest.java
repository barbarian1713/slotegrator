package StepDefinitions;

import Common.commonActions;
import Pages.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;

import static Common.config.HOLD_BROWSER_OPEN;

public class baseTest {
    protected WebDriver driver = commonActions.createDriver();
    protected basePage BasePage = new basePage(driver);
    protected loginPage LoginPage = new loginPage(driver);

@AfterClass
    void close() {
        if (!HOLD_BROWSER_OPEN){
            driver.close();
        }
    }


}
