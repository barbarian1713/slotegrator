package StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.time.Duration;

import static Constants.constant.BaseVariables.*;

public class loginSteps extends baseTest {


    @Given("the user is on login page")
    public void the_user_is_on_login_page() {
        BasePage.goToUrl(URL_SLOTEGRATOR);
    }

    @When("the user enter valid login")
    public void the_user_enter_valid_login() {
        LoginPage.enterValidLogin();
    }

    @When("the user enter valid password")
    public void the_user_enter_valid_password() {
        LoginPage.enterValidPassword();

    }

    @When("click submit")
    public void click_submit() {
        LoginPage.clickSubmit();
    }

    @Then("the user should be logged in successfully")
    public void the_user_should_be_logged_in_successfully() {
        LoginPage.headerLogoIsVisible();    }

    @When("click users")
    public void click_users() {
        LoginPage.clickUsers();
    }

    @When("click players")
    public void click_players() {
        LoginPage.clickPlayers();
    }

    @Then("check the table of players is visible")
    public void check_the_table_of_players_is_visible() {
        LoginPage.checkTableIsVisible();
    }

    @When("click sort")
    public void click_sort() {
        LoginPage.clickSortPlayersByName();
    }

    @Then("check the table of players sorted")
    public void check_the_table_of_players_sorted(){
        LoginPage.checkTableSorted();
    }

}