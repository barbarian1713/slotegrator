Feature: Authorization in admin
  Scenario: User should be able to login with valid credentials
    Given the user is on login page
    When the user enter valid login
    And the user enter valid password
    And click submit
    Then the user should be logged in successfully

